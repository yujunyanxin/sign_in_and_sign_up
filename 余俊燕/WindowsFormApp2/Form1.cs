﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace WindowsFormApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin", "123456");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key == uid && res.Value == pwd)
            {
                MessageBox.Show("登陆成功", "恭喜你", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登陆失败，请重新注册一个账号", "很遗憾", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Register register = new Register();
            register.Show();
        }
    }
}
