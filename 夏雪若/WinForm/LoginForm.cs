﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void LoginForm_Load(object sender, EventArgs e)
        {
            _user.Add("admin","123456");
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if(res.Key==uid && res.Value == pwd)
            {
                MessageBox.Show("登录成功！","恭喜您",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登陆失败！请返回重新登录或注册账号", "很遗憾", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            RegisterForm register = new RegisterForm();
            register.Show();
            this.Hide();
        }
    }
}
